# Products CRUD front end repo

## Description
This repo was created to be the frontend for the [nestjs-api-example](https://bitbucket.org/gaval_997/nestjs-api-example/) repo. This app will perform a basic CRUD interface for the Products entity.

## Build Setup
```bash
# Rename env-example file
$ mv ./env-example ./.env

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
