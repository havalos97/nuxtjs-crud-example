import { ProductStoreInterface } from './interfaces/ProductStoreInterface'

export const getDefaultProductModel = () => ({
  _id: '',
  name: '',
  description: '',
  price: 0,
  createdAt: '',
})

export const getNewProductModel = () => ({
  name: '',
  description: '',
  price: 0,
})

export const getDefaultState = (): ProductStoreInterface => ({
  productList: [],
  newProduct: getNewProductModel(),
  product: getDefaultProductModel(),
  productIdToDelete: '',
  productToUpdate: getDefaultProductModel(),
})

export default (): ProductStoreInterface => getDefaultState()
