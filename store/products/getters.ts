import { getterTree } from 'nuxt-typed-vuex'
import state from './state'

export default getterTree(state, {
  productList: (state) => state.productList,
  newProduct: (state) => state.newProduct,
  product: (state) => state.product,
  productIdToDelete: (state) => state.productIdToDelete,
  productToUpdate: (state) => state.productToUpdate,
})
