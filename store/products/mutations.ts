import { mutationTree } from 'nuxt-typed-vuex'
import state, { getDefaultState } from './state'
import { ProductInterface } from '~/store/products/interfaces/ProductInterface'

export default mutationTree(state, {
  SET_PRODUCT_LIST: (state, productList: ProductInterface[]) => {
    state.productList = productList.slice(0)
  },
  SET_PRODUCT: (state, newProduct: ProductInterface) => {
    state.product = { ...newProduct }
  },
  SET_NEW_PRODUCT: (state, newProduct: ProductInterface) => {
    state.newProduct = { ...newProduct }
  },
  SET_PRODUCT_ID_TO_DELETE: (state, productIdToDelete: string) => {
    state.productIdToDelete = productIdToDelete
  },
  SET_PRODUCT_TO_UPDATE: (state, newProductToUpdate: ProductInterface) => {
    state.productToUpdate = { ...newProductToUpdate }
  },
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
})
