import { actionTree } from 'nuxt-typed-vuex'
import axios from 'axios'
import state, { getDefaultProductModel, getNewProductModel } from './state'
import getters from './getters'
import mutations from './mutations'
import { ProductInterface } from '~/store/products/interfaces/ProductInterface'

const API_URL = process.env.API_URL

export default actionTree(
  { state, getters, mutations },
  {
    resetProductsState({ commit }) {
      commit('RESET_STATE')
    },
    async fetchProducts({ commit }) {
      commit('SET_PRODUCT_LIST', [])
      try {
        const { data } = await axios.get(`${API_URL}/product`)
        const productList: ProductInterface[] = data.map(
          (product: ProductInterface) => {
            return {
              _id: product._id,
              name: product.name,
              description: product.description,
              price: product.price,
              createdAt: product
                .createdAt!.split('T')
                .join(' ')
                .replace(/Z|\..*/g, ''),
            }
          }
        )
        commit('SET_PRODUCT_LIST', productList)
      } catch (exception) {}
    },
    async findProduct({ commit }, { productId, target }) {
      /* Reset ProductModel in case the axios request fails */
      const baseProduct = getDefaultProductModel()
      commit(target, baseProduct)

      try {
        const { data } = await axios.get(`${API_URL}/product/${productId}/`)
        const foundProduct: ProductInterface = {
          _id: data._id,
          name: data.name,
          description: data.description,
          price: data.price,
          createdAt: data
            .createdAt!.split('T')
            .join(' ')
            .replace(/Z|\..*/g, ''),
        }

        commit(target, foundProduct)
        return true
      } catch (exception) {}
      return false
    },
    async saveProduct({ commit, dispatch }, product: ProductInterface) {
      try {
        await axios.post(`${API_URL}/product/`, product)
      } catch (exception) {}
      /* Fetch new product list */
      await dispatch('fetchProducts')
      /* Reset New Product Form */
      commit('SET_NEW_PRODUCT', getNewProductModel())
    },
    async updateProduct({ dispatch }, productToUpdate: ProductInterface) {
      try {
        const productId = productToUpdate._id
        productToUpdate = {
          name: productToUpdate.name,
          description: productToUpdate.description,
          price: productToUpdate.price,
        }
        await axios.put(`${API_URL}/product/${productId}/`, productToUpdate)
      } catch (exception) {}
      /* Fetch new product list */
      await dispatch('fetchProducts')
    },
    async deleteProduct({ commit, dispatch }, productId) {
      try {
        await axios.delete(`${API_URL}/product/${productId}/`)
      } catch (exception) {}
      /* Fetch new product list */
      await dispatch('fetchProducts')
      /* Reset Product ID To Delete */
      commit('SET_PRODUCT_ID_TO_DELETE', '')
    },
  }
)
