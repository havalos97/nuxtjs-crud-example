import { ProductInterface } from '~/store/products/interfaces/ProductInterface'

export interface ProductStoreInterface {
  productList: ProductInterface[]
  newProduct: ProductInterface
  product: ProductInterface
  productIdToDelete: string
  productToUpdate: ProductInterface
}
