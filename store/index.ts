import { getAccessorType, mutationTree, actionTree } from 'typed-vuex'
import * as products from './products'

// State
export const state = (): Record<string, unknown> => ({})

// Getters
export const getters = {}

// Mutations
export const mutations = mutationTree(state, {})

// Actions
export const actions = actionTree({ state, getters, mutations }, {})

// This compiles to nothing and only serves to return the correct type of the accessor
export const accessorType = getAccessorType({
  state,
  getters,
  mutations,
  actions,
  modules: {
    products,
  },
})
